import re
import urllib.request as req

# collects all links to other wikipedia topics on the wiki page for the input topic
def getLinkedTopics(topic):
	url = "https://en.wikipedia.org/wiki/" + topic
	page = req.urlopen(url)
	message = str(page.read())
	expression = r'<a href="/wiki/' + r'.*?' + r'" title='
	p = re.compile(expression)
	links = p.findall(message)
	topics = []
	for link in links:
		candidate = link[15:-8]
		if candidate not in topics:
			valid = candidate.startswith("Portal:")
			valid = valid or candidate.startswith("Special:")
			valid = valid or candidate.startswith("Help:")
			valid = valid or candidate.startswith("Wikipedia:")
			valid = valid or candidate.startswith("File:")
			valid = valid or candidate.startswith("Template:")
			valid = valid or candidate.startswith("Talk:")
			valid = valid or candidate.startswith("User:")
			valid = valid or candidate.startswith("Category:")
			valid = valid or candidate.startswith("Wikipedia_talk:")
			valid = valid or candidate.startswith("Template_talk:")
			valid = valid or (candidate == topic)
			valid = valid or candidate.startswith(topic + '">')
			valid = valid or candidate.endswith(";action=edit")
			valid = valid or ("mw-disambig" in candidate)
			valid = not valid
			if candidate.endswith('class="mw-redirect'):
				candidate = candidate[:-20]
			if candidate.endswith('class="mw-disambig'):
				candidate = candidate[:-20]
			if valid:
				topics.append(candidate)
	return topics

#finds the shortest path between two topics through Wikipedia articles
def isWikiLinked(topic, targetTopic):
	return isWikiLinkedHelper([topic], targetTopic)

def isWikiLinkedHelper(frontier,targetTopic,checked=[],path={}):
	print("Looking at", frontier[0])
	
	newFrontier = frontier[1:]
	checked.append(frontier[0])
	newTopics = getLinkedTopics(frontier[0])
	for newTopic in newTopics:
		if newTopic == targetTopic:
			print("Found!")
			path[newTopic] = frontier[0]
			currentTopic = newTopic
			pathString = currentTopic
			while currentTopic in path.keys():
				pathString = path[currentTopic] + " -> " + pathString
				currentTopic = path[currentTopic]
			return pathString
			return True
		elif newTopic not in newFrontier and newTopic not in checked:
			path[newTopic] = frontier[0] #each topic leads back to the topic it came from
			newFrontier.append(newTopic)
	return isWikiLinkedHelper(newFrontier,targetTopic,checked,path)

while True:
        print()
        print("Write your topics exactly as they appear in the wiki url.")
        topic1 = input("Enter first topic: ").replace(" ", "_")
        topic2 = input("Enter second topic: ").replace(" ", "_")
        result = isWikiLinked(topic1, topic2)
        print()
        print(result)

