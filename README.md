
# WikiSearch

Takes in two input topics and finds the shortest path between them through Wikipedia articles. Based off [the Wiki Game](https://en.wikipedia.org/wiki/Wikipedia:Wiki_Game).

## Gödel to Finance

![](Media/godel-to-finance.png)

## Tuple to ZFC

![](Media/tuple-to-zfc.png)

## Computer Science to Philosophy

![](Media/cs-to-philosophy.png)

Note: Wikipedia puts a speed limit on how often you can access their servers, which caps the speed of this program.